import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from './pages/Home';
import Todo from './pages/Todo';
import TodoEdit from './pages/TodoEdit';
import Today from './pages/Today';
import Planned from './pages/Planned';
import Important from './pages/Important';
import Asc from './pages/Asc';
import Desc from './pages/Desc';
import AddList from './containers/AddList';
import List from './containers/List';
import EditList from './containers/EditList';
import ListShow from './pages/ListShow';
import Report from './pages/Report';
// import DateRange from './pages/DateRange';

import DateRange from './pages/DateRange';

import Header from './components/Header';
// import Filter from './components/Filter';

function App() {
  return (
    <Router>
      <div className="wrapper">
        <Header />
        {/* <Filter /> */}
        {/* <DateRange /> */}
        <main className="container-fluid py-4">
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/today">
              <Today />
            </Route>
            <Route exact path="/planned">
              <Planned />
            </Route>
            <Route exact path="/imp">
              <Important />
            </Route>
            <Route exact path="/asc">
              <Asc />
            </Route>
            <Route exact path="/desc">
              <Desc />
            </Route>
            <Route path="/add">
              <Todo />
            </Route>
            <Route exact path="/addlist">
              <AddList />
            </Route>
            <Route exact path="/list">
              <List />
            </Route>
            <Route exact path="/report">
              <Report />
            </Route>
            <Route path="/editlist/:id">
              <EditList />
            </Route>
            <Route path="/edit/:id">
              <TodoEdit />
            </Route>
            <Route path="/group/:id">
              <ListShow />
            </Route>
            <Route path="/daterange">
              <DateRange />
            </Route>
            <Route path="/coba">
              <AddList />
              <List />
            </Route>
          </Switch>
        </main>
      </div>
    </Router>
  );
}

export default App;
