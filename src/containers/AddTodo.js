import React, { Fragment, useState } from 'react';
import axios from 'axios';

import Form from '../components/Form';

export default function Todo() {
  const url = process.env.REACT_APP_API;

  let initialState = {
    kegiatan: '',
    tanggal: '',
    status: false,
    ket: '',
    imp: false,
    group_list: '',
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);

  const handleInput = (event) => {
    const { name, value } = event.target;
    if (name === 'group_list') {
      setData({...data, [name]: parseInt(value)})
    } else {
      setData({...data, [name]: value})
    }
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(data)

    setLoading(true);

    axios.post(`${url}`, data)
      .then(response => {
        console.log(response.data)
        setData(initialState)
        setLoading(false);
    })
      .catch(error => console.log(error.response))
  }

  return (
    <Fragment>
      <Form handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput}/>
    </Fragment>
  )
}
