import React, { Fragment, useState, useEffect } from 'react';
import { withRouter, useParams } from 'react-router-dom';
import axios from 'axios';

import Form_list from '../components/Form_list';

function EditList(props) {
  const url = process.env.REACT_APP_API_List;
	let { id } = useParams();

	let initialState = {
    group_list: ''
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
  	axios.get(`${url}${id}`)
  		.then(result => setData(result.data))
  }, [id]);

  const handleInput = (event) => {
    const { name, value } = event.target;

    setData({ ...data, [name]: value });
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();

    setLoading(true);

    axios.put(`${url}${id}`, data)
      .then(response => {
        console.log(response)
        setLoading(false);
        props.history.push('/');
      })
      .catch(error => console.log(error))
    
    setLoading(false);

  }

  return (
    <Fragment>
      <Form_list handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput} />
    </Fragment>
  )
}

export default withRouter(EditList);