import React, { useState, useEffect, Fragment, Component } from 'react';
import eye from '../images/eye.svg';
import { useRouteMatch, useLocation, Link } from 'react-router-dom';
import queryString from 'query-string';
import { Table } from 'react-bootstrap';

import axios from 'axios';

export default function List() {
  const url = process.env.REACT_APP_API_List;
  const pdf = process.env.REACT_APP_API_Report;
  const [data, setData] = useState([]);
  const query = queryString.parse(useLocation().search);
  const page = (query.page) ? query.page : '1';
  const match = useRouteMatch();
  const [halaman, setHalaman] = useState(null);


  useEffect(() => { 
    axios.get(`${url}?pageNumber=${page ?? '1'}`)
      .then(response => {
          setData(response.data);
          setHalaman(JSON.parse(response.headers['x-pagination']))
      })  
      .catch(error => console.log(error))
      },[page]);
  
  // viewHandler = async () => {
  //   axios(`${pdf}`, {
  //     method: "GET",
  //     responseType: "blob"
  //     //Force to receive data in a Blob Format
  //   })
  //     .then(response => {
  //       //Create a Blob from the PDF Stream
  //       const file = new Blob([response.data], {
  //         type: "application/pdf"
  //       });
  //       //Build a URL from the file
  //       const fileURL = URL.createObjectURL(file);
  //       //Open the URL on new Window
  //       window.open(fileURL);
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     });
  // };

  // const deleteList = (id) => {
  //   axios.delete(`${url}${id}`)
  //     .then(res => {
  //       // console.log(response);
  //       setData(data.filter(list => list.id !== id));
  //     })
  //     .catch(error => console.log(error))
  // }


  const deleteList = (id) => {
    console.log(id)
    axios.delete(`${url}${id}`)
      // console.log(response);
  }




  // const _delete = () => {

  //   if (window.confirm('Are you sure ?')) {
  //     deleteList(data.id);
  //   } else {
  //     console.log('cancel');
  //   }
  // }

  const pagination = () => {
    if (halaman !== null) {
      return (
        <nav aria-label="page navigation example">
          <ul className="pagination">
            <li className={`page-item ${halaman.HasPrev ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${match.url}?page=1`}>&laquo;</Link></li>
            {[...Array(halaman.TotalPages)].map((x, i) => {
              i++
              if (i === 1 || i === halaman.TotalPages || (i >= halaman.CurrentPage - 2 && i <= halaman.CurrentPage + 2)) {
                return (
                  <li key={i} className={`page-item ${halaman.CurrentPage === i ? 'active' : ''}`}>
                  <Link className="page-link" to={`${match.url}?page=${i}`}>{i}</Link></li>
                )}
            }
            )}
            <li className={`page-item ${halaman.HasNext ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${match.url}?page=${halaman.TotalPages}`}>&raquo;</Link></li>
          </ul>
        </nav>
      )
    }
  }

  

  return (
    <Fragment>
      <div className="container">
      <Link className="btn btn-primary" to={`/addlist`}>Add List </Link>
        <div className="row my-4">
            <Table striped bordered hover>
            <thead>
              <tr>
                <th>Nama List</th>
                <th colSpan="4">Action</th>
              </tr>
            </thead>
            <tbody>
            {data.map((listitem, index) => (
              <tr>
                <td>{listitem.group_list}</td>
                <td><Link className="dropdown-item" to={`/editlist/${listitem.id}`}>Edit</Link></td>
                <td><a className="dropdown-item" onClick={() => deleteList(listitem.id)} href="/list">Delete</a></td>
                <td><Link className="dropdown-item" to={`/group/${listitem.id}`}>Show Data</Link></td>
                <td><a className="dropdown-item" href={`${pdf}/list/${listitem.id}`}>Generate PDF</a></td>
              </tr>))}
            </tbody>
          </Table>
          <div> {pagination()} </div>
        </div>
      </div>
    </Fragment>
  )
}
