import React, { Fragment, useState } from 'react';
import axios from 'axios';

import Form from '../components/Form_list';

export default function List() {
  const url = process.env.REACT_APP_API_List;
  let initialState = {
    group_list: '',
  }

  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(false);


  const handleInput = (event) => {
    const { name, value } = event.target;

    setData({ ...data, [name]: value });
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();

    setLoading(true);

    axios.post(`${url}`, data)
      .then(response => {
        console.log(response.data)
        setData(initialState)
        setLoading(false);
        
      })
      .catch(error => {
        console.log(error.response)        
      })
  }

  return (
    <Fragment>
      <Form handleSubmit={handleSubmit} data={data} loading={loading} handleInput={handleInput} />
    </Fragment>
  )
}
