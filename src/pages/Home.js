import React, { Fragment } from 'react';

import ListCard from '../containers/ListCard';

export default function Home() {
  return (
    <Fragment>
      <h2>Dashboard</h2>
      <ListCard />
    </Fragment>
  )
}
