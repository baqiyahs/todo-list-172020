import React, { useState, useEffect, Fragment } from 'react';
import Card from '../components/Card';
import DatePicker from 'react-datepicker';

import { useHistory, useRouteMatch, useLocation, Link } from 'react-router-dom';
import queryString from 'query-string';
import moment from 'moment';

import axios from 'axios';
import "react-datepicker/dist/react-datepicker.css";

export default function ListCard(props) {
  let history = useHistory();

  let url = process.env.REACT_APP_API;
  const [data, setData] = useState([]);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  
  const query = queryString.parse(useLocation().search);
  const page = (query.page) ? query.page : '1';
  const match = useRouteMatch();
  const [search, setSearch] = useState('');
  const [halaman, setHalaman] = useState(null);
  const mindate = (query.mindate) ? query.mindate : '';
  const maxdate = (query.maxdate) ? query.maxdate : '';
  

  const handleSearch = (event) => {
     setSearch(event.target.value) 
  }

  const handleSubmit = (event) => {
     event.preventDefault()
     axios.get(url)
         .then (result => {
          setData(result.data)
          console.log(result)
         })
  }

  useEffect(() => {
  if (mindate !== '' && maxdate !== '') {
    url += `range?mindate=${mindate}&maxdate=${maxdate}&pageNumber=${page}`;
  } else {
    url += `?pageNumber=${page}`;
  }
    axios.get(url)
      .then(response => {
          setData(response.data);
          setHalaman(JSON.parse(response.headers['x-pagination']))
          console.log(url)
      })
      .catch(error => console.log(error))
      
      }, [url, page, mindate, maxdate]);

  const submitDateFilter = () => {
    let filter = `?page=${page}`;
    if (startDate !== null) {
      filter += `&mindate=${moment(startDate).format('YYYY-MM-DD')}`;
    }
    if (endDate !== null) {
      filter += `&maxdate=${moment(endDate).format('YYYY-MM-DD')}`;
    }

    history.push(filter);
  }

  const link = (hal) => {
    let link = `${match.url}`;

    if (mindate !== '' && maxdate !== '') {
      link += `?page=${hal}&mindate=${mindate}&maxdate=${maxdate}`;
    } else {
      link += `page=${hal}`;
    }
    return link;
  }



  const pagination = () => {
    if (halaman !== null) {
      return (
        <nav aria-label="page navigation example">
          <ul className="pagination">
            <li className={`page-item ${halaman.HasPrev ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${link(1)}`}>&laquo;</Link></li>
            {[...Array(halaman.TotalPages)].map((x, i) => {
              i++
              if ((i >= halaman.CurrentPage - 2 && i <= halaman.CurrentPage + 2)) {
                return (
                  <li key={i} className={`page-item ${halaman.CurrentPage === i ? 'active' : ''}`}>
                  <Link className="page-link" to={`${link(i)}`}>{i}</Link></li>
                )}
            }
            )}
            <li className={`page-item ${halaman.HasNext ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${link(halaman.TotalPages)}`}>&raquo;</Link></li>
          </ul>
        </nav>
      )
    }
  }

  const deleteTodo = (id) => {
    axios.delete(`${url}${id}`)
      .then(response => {
        // console.log(response);
        setData(data.filter(todo => todo.id !== id));
      })
      .catch(error => console.log(error))
  }

   const updateTodo = (todo) => {
    todo.status = !todo.status
    axios.put(`${url}UStatus/${todo.id}`, todo)
    .then (result => {
      console.log(result)
      setData([...data], result.data.todo) 
      })
  }

  const updateImpTodo = (todo) => {
    todo.imp = !todo.imp
    axios.put(`${url}UImp/${todo.id}`, todo)
    .then (result => {
      console.log(result)
      setData([...data], result.data.todo) 
      })
  }


  return (
    <Fragment>
    <div>
        <DatePicker
        selected={startDate}
        onChange={date => setStartDate(date)}
        selectsStart
        startDate={startDate}
        endDate={endDate}
        />
        <DatePicker
        selected={endDate}
        onChange={date => setEndDate(date)}
        selectsEnd
        startDate={startDate}
        endDate={endDate}
        minDate={startDate}
        />
        <button className= "btn btn-secondary" onClick={submitDateFilter}>Filter</button>
      </div>
      <div>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <input type="text" name="search" value={search} onChange={handleSearch}/>
            <button className= "btn btn-secondary" type="submit">Cari</button>
          </div>
        </form>
       </div>
      <div className="dropdown">
        <button type="button" className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sort By</button>
          <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <Link className="dropdown-item" to={`/asc`}>Sort By Asc</Link>
            <Link className="dropdown-item" to={`/desc`}>Sort By Desc</Link>
          </div>
      </div>
      <div className="row my-4">
        {data.map((todoitem, index) => (
          <Card key={index} todo={todoitem} deletetodo={deleteTodo} updatetodo={updateTodo} updateimptodo={updateImpTodo}/>
        ))}        
      </div>
      <div> {pagination()} </div>
    </Fragment>
  )
}
