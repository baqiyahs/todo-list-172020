import React, { useState, useEffect, Fragment } from 'react';
import Card from '../components/Card';
import * as Icon from 'react-feather' ;

import { useRouteMatch, useLocation, Link } from 'react-router-dom';
import queryString from 'query-string';

import axios from 'axios';

export default function Desc() {
  const url = process.env.REACT_APP_API;
  const [data, setData] = useState([]);
  const query = queryString.parse(useLocation().search);
  const page = (query.page) ? query.page : '1';
  const match = useRouteMatch();
  const [search, setSearch] = useState('');
  const [halaman, setHalaman] = useState(null);
 
  const handleSearch = (event) => {
     setSearch(event.target.value) 
  }

  const handleSubmit = (event) => {
     event.preventDefault()
     axios.get(`${url}Search/${search}`)
         .then (result => {
          setData(result.data)
          console.log(result)
         })
  }

  useEffect(() => { 
    axios.get(`${url}desc/?pageNumber=${page ?? '1'}`)
      .then(response => {
          setData(response.data);
          setHalaman(JSON.parse(response.headers['x-pagination']))
      })  
      .catch(error => console.log(error))
      }, [page]);

  
  const pagination = () => {
    if (halaman !== null) {
      return (
        <nav aria-label="page navigation example">
          <ul className="pagination">
            <li className={`page-item ${halaman.HasPrev ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${match.url}?page=1`}>&laquo;</Link></li>
            {[...Array(halaman.TotalPages)].map((x, i) => {
              i++
              if (i === 1 || i === halaman.TotalPages || (i >= halaman.CurrentPage - 2 && i <= halaman.CurrentPage + 2)) {
                return (
                  <li key={i} className={`page-item ${halaman.CurrentPage === i ? 'active' : ''}`}>
                  <Link className="page-link" to={`${match.url}?page=${i}`}>{i}</Link></li>
                )}
            }
            )}
            <li className={`page-item ${halaman.HasNext ? '' : 'disabled'}`}>
            <Link className="page-link" to={`${match.url}?page=${halaman.TotalPages}`}>&raquo;</Link></li>
          </ul>
        </nav>
      )
    }
  }

  const deleteTodo = (id) => {
    axios.delete(`${url}${id}`)
      .then(response => {
        // console.log(response);
        setData(data.filter(todo => todo.id !== id));
      })
      .catch(error => console.log(error))
  }

   const updateTodo = (todo) => {
    todo.status = !todo.status
    axios.put(`${url}UStatus/${todo.id}`, todo)
    .then (result => {
      console.log(result)
      setData([...data], result.data.todo) 
      })
  }

  const updateImpTodo = (todo) => {
    todo.imp = !todo.imp
    axios.put(`${url}UImp/${todo.id}`, todo)
    .then (result => {
      console.log(result)
      setData([...data], result.data.todo) 
      })
  }


  return (
    <Fragment>
      <div className="row my-4">
        <div className="col-8">
          <div className="dropdown">
            <button type="button" className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sort By</button>
              <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <Link className="dropdown-item" to={`/asc`}>Sort By Asc</Link>
                <Link className="dropdown-item" to={`/desc`}>Sort By Desc</Link>
              </div>
          </div>
        </div>
        <div className="col text-right">
          <form onSubmit={handleSubmit}>
            <div className="input-group">
              <input className="form-control" placeholder="Search" aria-label="Search" type="text" name="search" value={search} onChange={handleSearch}/>
              <button><Icon.Search color="black" size={20} /></button>
            </div>
          </form>
        </div>
      </div>
      <div className="row my-4">
        {data.map((todoitem, index) => (
          <Card key={index} todo={todoitem} deletetodo={deleteTodo} updatetodo={updateTodo} updateimptodo={updateImpTodo}/>
        ))}        
      </div>
      <div> {pagination()} </div>
    </Fragment>
  )
}
