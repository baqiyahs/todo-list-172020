import React, {Component, Fragment} from 'react';
import axios from 'axios';
// import Card from '../components/Card';

const url = process.env.REACT_APP_API_Report;


class Report extends Component {
    // state = {
    //     pdf : [],
    // }

    // getReport = () => {
    //     axios.get(`${url}`)
    //         .then((result) => {
    //          this.setState ({
    //             pdf: result.data
    //         })
    //     })
    // }
   
    // render(){
    //     return(
    //       <Fragment>
    //       {
    //         this.state.pdf.map(pdf =>{
    //           return <Card data={pdf} report={this.getReport} />
    //         })
    //       }
    //       </Fragment>
    //     )
    // }

    viewHandler = async () => {
      axios(`${url}`, {
        method: "GET",
        responseType: "blob"
        //Force to receive data in a Blob Format
      })
        .then(response => {
          //Create a Blob from the PDF Stream
          const file = new Blob([response.data], {
            type: "application/pdf"
          });
          //Build a URL from the file
          const fileURL = URL.createObjectURL(file);
          //Open the URL on new Window
          window.open(fileURL);
        })
        .catch(error => {
          console.log(error);
        });
    };
    render() {
      return (
        <div>
          <button className= "btn btn-secondary" onClick={this.viewHandler}> View Pdf </button>{" "}
        </div>
      );
    }
}
export default Report;



