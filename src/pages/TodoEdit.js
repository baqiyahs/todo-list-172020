import React, { Fragment } from 'react';

import EditTodo from '../containers/EditTodo';

export default function TodoEdit() {
  return (
    <Fragment>
      <h2>Todo Edit</h2>
      <EditTodo />      
    </Fragment>
  )
}