import React, { Fragment } from 'react';

import AddTodo from '../containers/AddTodo';

export default function Todo() {
  return (
    <Fragment>
      <h2>Add Todo</h2>
      <AddTodo />
    </Fragment>
  )
}
