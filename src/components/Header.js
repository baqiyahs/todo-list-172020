import React from 'react';
import Logo from './Logo';
import Menu from './Menu';

export default function Header() {
  return (
    <div className="navbar navbar-expand-lg navbar-dark bg-dark">
      <Logo title="TodoApp" />
      <Menu />
    </div>
  )
}