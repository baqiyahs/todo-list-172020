import React, { useState, useEffect, Fragment } from 'react';
import axios from 'axios';
import ReactShadowScroll from 'react-shadow-scroll';

export default function Form({handleSubmit, handleInput, data, loading}) {
  const url = process.env.REACT_APP_API_List;
  const [list, setList] = useState([]);
  const urlist = process.env.REACT_APP_API_List;

  useEffect(() => {
    axios.get(`${urlist}`)
      .then(response => {
      setList(response.data);
      console.log(response.data);
      })  
      .catch(error => console.log(error))
      }, []);


  return (
    <Fragment>
      <div className="container">
        <div className="row my-4">
          <div className="col col-md-4">
            <form onSubmit={handleSubmit}>
              <div className="form-group">
                <label>Kegiatan</label>
                <input type="text" name="kegiatan" value={data.kegiatan} className="form-control" onChange={handleInput} />
              </div>
              <div className="form-group">
                <label>Tanggal</label>
                <input id="date" type="date" name="tanggal" value={data.tanggal} className="form-control" onChange={handleInput} />
              </div>
              <div className="form-group">
                <label>Keterangan</label>
                <input type="text" name="ket" value={data.ket} className="form-control" onChange={handleInput} />
              </div>
              <div className="form-group">
                <label for="sel1">Nama List</label>
                <ReactShadowScroll>
                  <select class="form-control" id="sel1" name ="group_list" onChange={handleInput}>
                    <option value="" disabled selected>Pilih</option>
                      {list.map((listitem, index) => 
                    <option value={listitem.id}>{listitem.group_list}</option>)}
                  </select>
                  </ReactShadowScroll>
              </div>
              <button className="btn btn-primary" type="submit" disabled={loading}>Simpan</button>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  )
}