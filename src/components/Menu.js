import React from 'react';
import { Link } from 'react-router-dom';

export default function Menu() {
  let listMenu = [
    {url: '/', title: 'Tasks'},
    {url: '/add', title: 'Add Todo'},
    {url: '/planned', title: 'Planned'},
    {url: '/today', title: 'Today'},
    {url: '/imp', title: 'Important'},
    {url: '/list', title: 'List'},
    {url: '/daterange', title: 'Date Range'},
    {url: '/report', title: 'Report'},
  ]

  return (
    <ul className="navbar-nav mr-auto flex-row">
      {listMenu.map((list, index) => (
        <li key={index} className="nav-item">
          <Link className="nav-link px-2" to={list.url}>{list.title}</Link>
        </li>
      ))}
    </ul>
  )
}