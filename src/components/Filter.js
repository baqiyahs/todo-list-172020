import React, { Component } from 'react';
import DatePicker from 'react-date-picker';

class Filter extends Component {
  state = {
    date: new Date(),
  }
 
  onChange = date => this.setState({ date })
 
  render() {
    return (
      <div>
        <DatePicker
          selectionType='range'
          minimumDate={new Date()}
          onChange={this.onChange}
          value={this.state.date}
        />
      </div>
    );
  }
}
export default Filter;