import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table } from 'react-bootstrap';

export default function TableList(props) {
   const [data, setData] = useState(props.list);

  const _delete = (event) => {
    event.preventDefault();

    if (window.confirm('Are you sure ?')) {
      props.deletelist(props.list.id);
    } else {
      console.log('cancel');
    }
  }

  return (
      <div>
       <Table striped bordered hover>
          <thead>
            <tr>
              <th>Nama List</th>
              <th colSpan="3">Action</th>
            </tr>
          </thead>
          <tbody>
          {data.map((listitem, index) => (
            <tr>
              <td>{listitem.group_list}</td>
              <td><Link className="dropdown-item" to={`/editlist/${listitem.id}`}>Edit</Link></td>
              <td><a className="dropdown-item" href="/" onClick={_delete}>Delete</a></td>
              <td><a className="dropdown-item" href="/" onClick={_delete}>Show Data</a></td>
            </tr>))}
          </tbody>
        </Table>
      </div>
  )
}
