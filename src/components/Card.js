import React, { useState, useEffect } from 'react';
import icons from '../images/three-dots-vertical.svg';
import star from '../images/star.svg';
import starFill from '../images/star-fill.svg';
import { Link } from 'react-router-dom';
import './Card.css';

export default function Card(props) {
  const [menu, setMenu] = useState(false);
  const [dropdown, setDropdown] = useState();

  const _updateStatus = () => {
    props.updatetodo(props.todo)
  }

  const _updateImp = () => {
    props.updateimptodo(props.todo)
  }

  const showMenu = () => {
    setMenu(true);
  }

  useEffect(() => {
    const closeMenu = (event) => {
      if (!dropdown.contains(event.target)) {
        setMenu(false);
        document.removeEventListener('click', closeMenu);
      }
    }

    if(menu) {
      document.addEventListener('click', closeMenu);
    }

    return () => {
      if(menu) {
        document.removeEventListener('click', closeMenu);
      }
    }
  }, [menu, dropdown])

  const _delete = (event) => {
    event.preventDefault();

    if (window.confirm('Are you sure ?')) {
      props.deletetodo(props.todo.id);
      setMenu(false);
    } else {
      console.log('cancel');
    }
  }


  return (
    <div className="col col-md-4 col-xl-2 mb-5">
      {/* <div className="card-shadow" style= {{minHeight: '300px'}}> */}
        <div className="card-body">
          <div className="d-flex align-items-center justify-content-between pb-5">
            <span className="border-secondary p-2 rounded-pill">{props.todo.status ? 'Selesai' : 'Belum dikerjakan'}</span>
            <div className="dropdown">
              <button className="btn p-0" onClick={showMenu}>
                <img src={icons} alt="options" width="24" height="24" />
              </button>
              <div ref={(element) => setDropdown(element)} style={{minWidth: 0}} className={`dropdown-menu dropdown-menu-right ${menu ? 'show' : ''}`} aria-labelledby="dropdownMenuButton">
                <Link className="dropdown-item" to={`/edit/${props.todo.id}`}>Edit</Link>
                <a className="dropdown-item" href="/" onClick={_delete}>Delete</a>
              </div>
            </div>
          </div>
          <div className="mt-5">
            <h6 className="font-weight-bold">{props.todo.group_list}</h6>
            <h6 className="font-weight-light">{props.todo.tanggal}</h6>
            <h4 className="font-weight-bold">{props.todo.kegiatan}</h4>
            <p>{props.todo.ket}</p>
            <button className="btn-primary p-2" onClick={_updateStatus}>{props.todo.status ? 'Not Yet' : 'Done'}</button>
            <button className="btn" onClick={_updateImp}>
            {props.todo.imp ? 
              (<img src={starFill} width="20" height="20" />) 
              : 
              (<img src={star} width="20" height="20" />)
            }
            </button>
          </div>
        </div>
      {/* </div> */}
    </div>
  )
}