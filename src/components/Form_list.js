import React, { Fragment } from 'react';

export default function Form({handleSubmit, data, handleInput, loading}) {
  return (
    <Fragment>
      <div className="container">
        <div className="row my-4">
          <div className="col col-md-4">
            <form onSubmit={handleSubmit}>
              <div className="form-group">
                <label>Nama List</label>
                <input type="text" name="group_list" value={data.group_list} className="form-control" onChange={handleInput} />
              </div>
              <button className="btn btn-primary" type="submit" disabled={loading}>Simpan</button>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  )
}